import React, { Component } from "react";
import IconFB from "./../../assets/img/icon_facebook.png";
import IconIG from "./../../assets/img/icon_instagram.png";
import IconTWC from "./../../assets/img/icon_twitch.png";
import IconTW from "./../../assets/img/icon_twitter.png";
import IconMail from "./../../assets/img/icon_mail.png";

class Footer extends Component {
  render() {
    return (
      <footer>
        <div className="container">
          <div className="row">
            <div className="col-lg-4">
              <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
              <p>binaracademy@gmail.com</p>
              <p>081-233-334-808</p>
            </div>
            <div className="col-lg-2" id="servis">
              <p>Our Services</p>
              <p>Why Us</p>
              <p>Testimonial</p>
              <p>FAQ</p>
            </div>
            <div className="col-lg-4">
              <p>Connect With Us</p>
              <img src={IconFB} alt="" className="img-logo" />
              <img src={IconIG} alt="" className="img-logo" />
              <img src={IconTW} alt="" className="img-logo" />
              <img src={IconMail} alt="" className="img-logo" />
              <img src={IconTWC} alt="" className="img-logo" />
            </div>
            <div className="col-lg-2">
              <p>Copyright Binar 2022</p>
              <div className="rectangle"></div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;

async function fetchDataCars() {
  try {
    let cars = await fetch("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json");
    let data = await cars.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}
export default fetchDataCars;

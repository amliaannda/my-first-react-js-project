import { combineReducers } from "redux";
import CarsReducers from "./CarsReducers";

const index = combineReducers({
  cars: CarsReducers,
});

export default index;

import { FILTERING_CARS } from "../actions/CarsActions";

export default function carsReducers(state = { data: [] }, action) {
  switch (action.type) {
    case FILTERING_CARS:
      return { ...state, data: action.payload };
    default:
      return state;
  }
}

import fetchDataCars from "../../components/fetchCarsData";
import axios from "axios";
export const FILTERING_CARS = "FILTERING_CARS";
// Kak reka

export const FilteringCars =
  ({ jumlahPenumpang, tanggal, waktu }) =>
  async (dispatch) => {
    try {
      const res = await axios.get("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json");
      console.log(waktu);
      if (jumlahPenumpang !== "" && tanggal !== "" && waktu !== "") {
        const data = res.data.filter((item) => {
          const filterpenumpang = parseInt(item.capacity) >= parseInt(jumlahPenumpang);
          const dateTime = new Date(`${tanggal} ${waktu.substr(0, 5)}`);
          const beforeEpochTime = dateTime.getTime();
          const itemDate = new Date(item.availableAt);
          const filterdate = itemDate.getTime() <= beforeEpochTime;
          return filterpenumpang && filterdate;
        });
        dispatch({
          type: "FILTERING_CARS",
          payload: data,
        });
      } else if (jumlahPenumpang !== "") {
        const data = res.data.filter((item) => {
          const filterpenumpang = parseInt(item.capacity) >= parseInt(jumlahPenumpang);
          return filterpenumpang;
        });
        dispatch({
          type: "FILTERING_CARS",
          payload: data,
        });
      } else if (tanggal !== "" && waktu !== "") {
        const data = res.data.filter((item) => {
          const dateTime = new Date(`${tanggal} ${waktu.substr(0, 5)}`);
          const beforeEpochTime = dateTime.getTime();
          const itemDate = new Date(item.availableAt);
          const filterdate = itemDate.getTime() <= beforeEpochTime;
          return filterdate;
        });
        dispatch({
          type: "FILTERING_CARS",
          payload: data,
        });
      } else {
        dispatch({
          type: "FILTERING_CARS",
          payload: res.data,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

import React, { Component } from "react";
import ImageCars from "../../assets/img/img_car.png";
import ImageService from "../../assets/img/img_service.png";
import ImageChecklist from "../../assets/img/checklist.png";
import IconComplete from "../../assets/img/icon_complete.png";
import IconPrice from "../../assets/img/icon_price.png";
import IconTime from "../../assets/img/icon_24hrs.png";
import IconProf from "../../assets/img/icon_professional.png";
import IconStar from "../../assets/img/star.png";
import ImageUpin from "../../assets/img/foto-upin.jpeg";
import ImgJohnDee from "../../assets/img/img_photo.png";
import ImgCewe from "../../assets/img/img_cewe.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import Button from "../../components/button/Button";

class Index extends Component {
  render() {
    return (
      <div>
        <div className="konten1">
          <div className="container konten-1">
            <div className="row">
              <div className="col-lg-5">
                <h2>
                  <strong>Sewa & Rental Mobil Terbaik di kawasan Palembang</strong>
                </h2>
                <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                <Button />
              </div>
              <div className="col-lg-7">
                <img src={ImageCars} alt="gambar mobil" className="mobil" />
              </div>
            </div>
          </div>
        </div>
        <div className="konten2 overflow" id="ourservice">
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <img className="img-fluid" src={ImageService} alt="gambar servis" />
              </div>
              <div className="col-md-6">
                <h4>
                  <strong>Best Car Rental for any kind of trip in Palembang!</strong>
                </h4>
                <p>Sewa mobil di Palembang bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                <ul className="list-group">
                  <li>
                    <span>
                      <img src={ImageChecklist} alt="check" />
                    </span>
                    Sewa Mobil Dengan Supir di Bali 12 Jam
                  </li>
                  <li>
                    <span>
                      <img src={ImageChecklist} alt="check" />
                    </span>
                    Sewa Mobil Lepas Kunci di Bali 12 Jam
                  </li>
                  <li>
                    <span>
                      <img src={ImageChecklist} alt="check" />
                    </span>
                    Sewa Mobil Jangka Panjang Bulanan
                  </li>
                  <li>
                    <span>
                      <img src={ImageChecklist} alt="check" />
                    </span>
                    Gratis Antar - Jemput Mobil di Bandara
                  </li>
                  <li>
                    <span>
                      <img src={ImageChecklist} alt="check" />
                    </span>
                    Layanan Airport Transfer / Drop In Out
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="konten3" id="whyus">
          <div className="container">
            <h4>
              <strong>Why Us?</strong>
            </h4>
            <p>Mengapa harus pilih Binar Car Rental?</p>
            <div className="row">
              <div className="col-lg-3 col-md-6 col-sm-12">
                <div className="card">
                  <div className="card-body">
                    <img src={IconComplete} alt="icon_complete" />
                    <h5 className="card-title">Mobil Lengkap</h5>
                    <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <div className="card">
                  <div className="card-body">
                    <img src={IconPrice} alt="icon_price" />
                    <h5 className="card-title">Harga Murah</h5>
                    <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <div className="card">
                  <div className="card-body">
                    <img src={IconTime} alt="icon_24hrs" />
                    <h5 className="card-title">Layanan 24 Jam</h5>
                    <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <div className="card">
                  <div className="card-body">
                    <img src={IconProf} alt="icon_professional" />
                    <h5 className="card-title">Sopir Profesional</h5>
                    <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="konten4" id="testimonials">
          <center>
            <h2>
              <strong>Testimonial</strong>
            </h2>
            <p>Beberapa review positif dari pelanggan kami</p>
          </center>
          <div className="overflow">
            <div id="carouselExampleControlsNoTouching" className="carousel slide" data-bs-touch="false" data-bs-interval="false">
              <div className="carousel-inner">
                <center>
                  <div className="carousel-item active">
                    <div className="row">
                      <div className="col-md-4 col-12 col-lg-4 dihide">
                        <div className="card">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-lg-3">
                                <div className="img">
                                  <img src={ImageUpin} alt="photo1" className="rounded-circle" />
                                </div>
                              </div>
                              <div className="col-lg-8" id="bintang-not-active">
                                <div className="star">
                                  <img src={IconStar} alt="star1" />
                                  <img src={IconStar} alt="star2" />
                                  <img src={IconStar} alt="star3" />
                                  <img src={IconStar} alt="star4" />
                                  <img src={IconStar} alt="star5" />
                                </div>
                                <br />
                                <p>
                                  “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                  do eiusmod”
                                </p>
                                <p>
                                  <b>Ipin 6, Durian Runtuh</b>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 col-12 col-lg-4">
                        <div className="card active">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-lg-3">
                                <div className="img">
                                  <img src={ImgJohnDee} alt="photo2" className="rounded-circle" />
                                </div>
                              </div>
                              <div className="col-lg-8" id="bintang">
                                <div className="star">
                                  <img src={IconStar} alt="star1" />
                                  <img src={IconStar} alt="star2" />
                                  <img src={IconStar} alt="star3" />
                                  <img src={IconStar} alt="star4" />
                                  <img src={IconStar} alt="star5" />
                                </div>
                                <br />
                                <p>
                                  “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                  do eiusmod”
                                </p>
                                <p>
                                  <b>John Dee, Bromo</b>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 col-12 col-lg-4 dihide">
                        <div className="card">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-lg-3">
                                <div className="img">
                                  <img src={ImgCewe} alt="photo3" className="rounded-circle" />
                                </div>
                              </div>
                              <div className="col-lg-8" id="bintang-not-active">
                                <div className="star">
                                  <img src={IconStar} alt="star1" />
                                  <img src={IconStar} alt="star2" />
                                  <img src={IconStar} alt="star3" />
                                  <img src={IconStar} alt="star4" />
                                  <img src={IconStar} alt="star5" />
                                </div>
                                <br />
                                <p>
                                  “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                  do eiusmod”
                                </p>
                                <p>
                                  <b>Laura 32, Bromo</b>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="carousel-item">
                    <div className="row">
                      <div className="col-md-4 col-12 col-lg-4 dihide">
                        <div className="card">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-lg-3">
                                <div className="img">
                                  <img src={ImgJohnDee} alt="photo1" className="rounded-circle" />
                                </div>
                              </div>
                              <div className="col-lg-8" id="bintang-not-active">
                                <div className="star">
                                  <img src={IconStar} alt="star1" />
                                  <img src={IconStar} alt="star2" />
                                  <img src={IconStar} alt="star3" />
                                  <img src={IconStar} alt="star4" />
                                  <img src={IconStar} alt="star5" />
                                </div>
                                <br />
                                <p>
                                  “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                  do eiusmod”
                                </p>
                                <p>
                                  <b>John Dee 32, Bromo</b>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 col-12 col-lg-4">
                        <div className="card active">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-lg-3">
                                <div className="img">
                                  <img src={ImgCewe} alt="photo2" className="rounded-circle" />
                                </div>
                              </div>
                              <div className="col-lg-8" id="bintang">
                                <div className="star">
                                  <img src={IconStar} alt="star1" />
                                  <img src={IconStar} alt="star2" />
                                  <img src={IconStar} alt="star3" />
                                  <img src={IconStar} alt="star4" />
                                  <img src={IconStar} alt="star5" />
                                </div>
                                <br />
                                <p>
                                  “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                  do eiusmod”
                                </p>
                                <p>
                                  <b>Laura 32, Bromo</b>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 col-12 col-lg-4 dihide">
                        <div className="card">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-lg-3">
                                <div className="img">
                                  <img src={ImageUpin} alt="photo3" className="rounded-circle" />
                                </div>
                              </div>
                              <div className="col-lg-8" id="bintang-not-active">
                                <div className="star">
                                  <img src={IconStar} alt="star1" />
                                  <img src={IconStar} alt="star2" />
                                  <img src={IconStar} alt="star3" />
                                  <img src={IconStar} alt="star4" />
                                  <img src={IconStar} alt="star5" />
                                </div>
                                <br />
                                <p>
                                  “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                  do eiusmod”
                                </p>
                                <p>
                                  <b>Ipin 6, Durian Runtuh</b>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="carousel-item">
                    <div className="row">
                      <div className="col-md-4 col-12 col-lg-4 dihide">
                        <div className="card">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-lg-3">
                                <div className="img">
                                  <img src={ImgCewe} alt="photo1" className="rounded-circle" />
                                </div>
                              </div>
                              <div className="col-lg-8" id="bintang-not-active">
                                <div className="star">
                                  <img src={IconStar} alt="star1" />
                                  <img src={IconStar} alt="star2" />
                                  <img src={IconStar} alt="star3" />
                                  <img src={IconStar} alt="star4" />
                                  <img src={IconStar} alt="star5" />
                                </div>
                                <br />
                                <p>
                                  “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                  do eiusmod”
                                </p>
                                <p>
                                  <b>Laura 32, Bromo</b>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 col-12 col-lg-4">
                        <div className="card  active">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-lg-3">
                                <div className="img">
                                  <img src={ImageUpin} alt="photo2" className="rounded-circle" />
                                </div>
                              </div>
                              <div className="col-lg-8" id="bintang">
                                <div className="star">
                                  <img src={IconStar} alt="star1" />
                                  <img src={IconStar} alt="star2" />
                                  <img src={IconStar} alt="star3" />
                                  <img src={IconStar} alt="star4" />
                                  <img src={IconStar} alt="star5" />
                                </div>
                                <br />
                                <p>
                                  “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                  do eiusmod”
                                </p>
                                <p>
                                  <b>Ipin 6, Durian Runtuh</b>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 col-12 col-lg-4 dihide">
                        <div className="card">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-lg-3">
                                <div className="img">
                                  <img src={ImgJohnDee} alt="photo3" className="rounded-circle" />
                                </div>
                              </div>
                              <div className="col-lg-8" id="bintang-not-active">
                                <div className="star">
                                  <img src={IconStar} alt="star1" />
                                  <img src={IconStar} alt="star2" />
                                  <img src={IconStar} alt="star3" />
                                  <img src={IconStar} alt="star4" />
                                  <img src={IconStar} alt="star5" />
                                </div>
                                <br />
                                <p>
                                  “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                  do eiusmod”
                                </p>
                                <p>
                                  <b>John Dee 32, Bromo</b>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </center>
              </div>
            </div>
          </div>
          <div className="carousel-control">
            <button className="carousel-control__prev" type="button" data-bs-target="#carouselExampleControlsNoTouching" data-bs-slide="prev">
              <span>
                <FontAwesomeIcon icon={faAngleLeft} />
                {/* <i className="fa-solid fa-angle-left"></i> */}
              </span>
            </button>
            <button className="carousel-control__next" type="button" data-bs-target="#carouselExampleControlsNoTouching" data-bs-slide="next">
              <span>
                <FontAwesomeIcon icon={faAngleRight} />
                {/* <i className="fa-solid fa-angle-right"></i> */}
              </span>
            </button>
          </div>

          <div className="konten5">
            <div className="container">
              <div className="kotak">
                <div className="isi-konten">
                  <h2>
                    <strong>Sewa Mobil di Palembang Sekarang</strong>
                  </h2>
                  <br />
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                  <br />
                  <Button />
                </div>
              </div>
            </div>
          </div>
          <div className="konten6" id="faq">
            <div className="container">
              <div className="row">
                <div className="col-md-5">
                  <h4>
                    <strong>Frequently Asked Question</strong>
                  </h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                </div>
                <div className="col-md-7">
                  <div className="accordion" id="accordionExample">
                    <div className="accordion-item border border-1">
                      <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Apa saja syarat yang dibutuhkan?
                        </button>
                      </h2>
                      <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                          <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel odit rerum voluptatum, laboriosam animi laborum veniam vero, enim sapiente commodi excepturi. Laborum accusantium nulla suscipit. Esse sequi quasi ipsa
                            iste?
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="accordion-item border border-1 mt-3">
                      <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          Berapa hari minimal sewa mobil lepas kunci?
                        </button>
                      </h2>
                      <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                          <p>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vero voluptatibus excepturi quo aspernatur beatae culpa maiores officiis amet laboriosam aperiam fugiat et eum eaque repudiandae animi illum quia, nobis
                            corrupti?
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="accordion-item border border-1 mt-3">
                      <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Berapa hari sebelumnya sabaiknya booking sewa mobil?
                        </button>
                      </h2>
                      <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                          <p>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat officia tempora voluptate ex, rem enim eos adipisci, explicabo doloribus obcaecati vero quod reprehenderit, odit ipsa? Aliquam quibusdam
                            dignissimos mollitia alias!
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="accordion-item border border-1 mt-3">
                      <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                          Apakah Ada biaya antar-jemput?
                        </button>
                      </h2>
                      <div id="collapseFour" className="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                          <p>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat officia tempora voluptate ex, rem enim eos adipisci, explicabo doloribus obcaecati vero quod reprehenderit, odit ipsa? Aliquam quibusdam
                            dignissimos mollitia alias!
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="accordion-item border border-1 mt-3">
                      <h2 className="accordion-header">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                          Bagaimana jika terjadi kecelakaan
                        </button>
                      </h2>
                      <div id="collapseFive" className="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                          <p>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat officia tempora voluptate ex, rem enim eos adipisci, explicabo doloribus obcaecati vero quod reprehenderit, odit ipsa? Aliquam quibusdam
                            dignissimos mollitia alias!
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Index;

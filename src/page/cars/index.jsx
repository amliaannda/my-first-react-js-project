import React, { Component } from "react";
import "../../assets/css/cars.css";
import ImageCars from "../../assets/img/img_car.png";
import Cards from "../../components/Cards/Cards";
import { connect } from "react-redux";
import { FilteringCars } from "../../store/actions";
class Cars extends Component {
  constructor(props) {
    super(props);

    this.state = {
      driver: "",
      date: "",
      time: "",
      capacity: "",
    };
  }

  handleDriver = (e) => {
    this.setState({
      driver: e.target.value,
    });
  };

  handleDate = (e) => {
    this.setState({
      date: e.target.value,
    });
  };

  handleTime = (e) => {
    this.setState({
      time: e.target.value,
    });
  };

  handleCapacity = (e) => {
    this.setState({
      capacity: e.target.value,
    });
  };
  handleSubmit = (e) => {
    let obj = {
      tanggal: this.state.date,
      jumlahPenumpang: this.state.capacity,
      waktu: this.state.time,
    };
    this.props.FilteringCars(obj);
  };

  render() {
    return (
      <div>
        <div className="konten1">
          <div className="container konten-1">
            <div className="row" id="content-1">
              <div className="col-lg-5">
                <h2>
                  <strong>Sewa & Rental Mobil Terbaik di kawasan Palembang</strong>
                </h2>
                <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                <br />
              </div>
              <div className="col-lg-7">
                <img src={ImageCars} alt="gambar mobil" className="mobil" />
              </div>
            </div>
            <div className="floating-form" id="floating-form">
              <div className="container">
                <div className="card shadow-sm p-2">
                  <div className="card-body">
                    <div className="form-display">
                      <div className="form-field">
                        <label for="driver" className="form-label">
                          Tipe Driver
                        </label>
                        <select className="form-select" name="driver" id="driver" onChange={(e) => this.handleDriver(e)}>
                          <option value="" disabled selected hidden>
                            Pilih Tipe Driver
                          </option>
                          <option>Dengan Sopir</option>
                          <option>Tanpa Sopir (Lepas Kunci)</option>
                        </select>
                      </div>
                      <div className="form-field">
                        <label for="date" className="form-label">
                          Tanggal
                        </label>
                        <input type="date" className="form-control date-label" name="date" id="date" placeholder="Pilih Tanggal" onChange={(e) => this.handleDate(e)} />
                      </div>
                      <div className="form-field">
                        <label for="time" className="form-label">
                          Waktu Jemput/Ambil
                        </label>
                        <select className="form-select clock-label" name="time" id="time" onChange={(e) => this.handleTime(e)}>
                          <option value="" disabled selected hidden>
                            Pilih Waktu
                          </option>
                          <option value="08:00 WIB">
                            <span id="span-punya-amalia">08.00</span> <span id="span-punya-ananda">WIB</span>
                          </option>
                          <option value="09:00 WIB">09.00 WIB</option>
                          <option value="10:00 WIB">10.00 WIB</option>
                          <option value="11:00 WIB">11.00 WIB</option>
                          <option value="12:00 WIB">12.00 WIB</option>
                        </select>
                      </div>
                      <div className="form-field">
                        <label for="penumpang" className="form-label">
                          Jumlah Penumpang(optional)
                        </label>
                        <input type="text" className="form-control penumpang-label" name="penumpang" id="penumpang" placeholder="Jumlah Penumpang" onChange={(e) => this.handleCapacity(e)} />
                      </div>
                      <div className="form-field">
                        <button className="btn btn-success" id="load-btn" value="Submit" onClick={(e) => this.handleSubmit(e)}>
                          Cari Mobil
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container" id="list-cars">
          <button id="clear-btn">Clear</button>
          <div id="cars-container">
            {/* Cards */}
            <Cards />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(null, { FilteringCars })(Cars);

import { Link, Route, Routes } from "react-router-dom";
import "./App.css";
import Footer from "./components/Footer/Footer";
import Navbar from "./components/Navbar/Navbar";
import Cars from "./page/cars/index";
import Index from "./page/index";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<Index />} />
        <Route path="/index" element={<Cars />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
